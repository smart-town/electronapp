const {ipcMain} = require('electron')
const { config } = require('./global')
const { initialUpdater } = require('./update')

function initialIPC() {
    ipcMain.on('message', (e, ...data) => {
        console.log("MainGetMessage", data)
    }),
    ipcMain.on('reload', (e, ...data) => {
        config.mainWindow.reload()
    }),
    ipcMain.on('update', (e) => {
        initialUpdater()
    })
}

module.exports = {
    initialIPC
}