const {app} = require("electron")
const path = require('path')
const appRoot = app.getAppPath()
const icon = path.resolve(appRoot, 'static/cherry.ico')
const entryUrl = path.resolve(appRoot, 'index.html')
const config = {
    app: 'rapp',
    mainWindow: null,
    renderRoot: path.resolve(appRoot)
}
module.exports = {
    icon,
    entryUrl,
    config,
}