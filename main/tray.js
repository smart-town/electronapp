const {Menu, Tray, app} = require("electron")
const {icon} = require("./global")

const trayMenu = Menu.buildFromTemplate([
    {label: 'exit', click: () => app.quit()}
])
function createTray() {
    const tray = new Tray(icon)
    tray.setContextMenu(trayMenu)
    tray.setToolTip('^_^')
}


module.exports = {
    createTray,
}