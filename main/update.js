const path = require('path')
const os = require('os')
const fs = require('fs')
const { dialog } = require('electron')
const { config } = require('./global')
const compressing = require('compressing')
function checkUpdate() {
    const checkPath = path.join(os.homedir(), 'Desktop', 'update.zip')
    const destPath = path.join(config.renderRoot, 'update.zip')
    if (fs.existsSync(checkPath)) {
        dialog.showMessageBox(config.mainWindow, {
            message: '有最新版本，是否更新',
            buttons: ['cancel', 'sure']
        }).then((v) => {
            const response = v.response
            console.log("ope result:", v)
            if (response === 1) {
                console.log(">>>!!! 开始下载，复制到" + destPath)
                fs.copyFile(checkPath, destPath, () => {
                    console.log("内容已复制，准备解压覆盖:")
                    compressing.zip.uncompress(destPath, config.renderRoot)
                        .then(v => {
                            console.log("解压成功....")
                        }).catch( e => {
                            console.error("解压失败", e)
                        })
                    }
                )
            } else {
                console.log(">>>>>cancel.....")
            }
        }).catch( e => {
            console.error("更新失败:", e)
        })
    } else {
        dialog.showMessageBox(config.mainWindow, {
            message: '已经是最新的了!'
        })
    }
}
function initialUpdater() {
    checkUpdate()
}

module.exports = {
    initialUpdater
}
