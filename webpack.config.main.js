module.exports = {
    entry: './main.js',
    module: {
        rules: [
          {
            // We're specifying native_modules in the test because the asset
            // relocator loader generates a "fake" .node file which is really
            // a cjs file.
            test: /native_modules\/.+\.node$/,
            use: 'node-loader',
          },
          {
            test: /\.(m?js|node)$/,
            parser: { amd: false },
            use: {
              loader: '@vercel/webpack-asset-relocator-loader',
              options: {
                outputAssetBase: 'native_modules',
              },
            },
          },
          {
            test: /\.(png|jpg|gif|svg|ico)$/,
            use: [
              {
                loader: 'file-loader',
                options: {},
              },
            ],
          },
        ]
    }
}