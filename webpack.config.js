const path = require("path")
const CopyWebpackPlugin = require("copy-webpack-plugin")
module.exports = {
    mode: 'development',
    devtool: 'eval-source-map',
    entry: {
        app: {
            import: './rapp/app.tsx',
            // dependOn: ['vendors'],
        },
        // vendors: ['react', 'react-dom', '@mui/material', 'zustand']
    },
    /* output: {
        path: path.resolve(__dirname, 'dist/rapp'),
        filename: '[name].js',
        sourceMapFilename: '[file].map'
    }, */
    externals: {
        'react': 'React',
        'react-dom': 'ReactDOM',
    },
    resolve: {
        extensions: [".ts", ".tsx",".js", ".json", ".jsx", ".css"],
        alias: {
        }
    },
    module: {
        rules: [
            {
                test: /\.(t|j)sx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'ts-loader',
                        options: {
                            transpileOnly: false,
                        }
                    },
                ]
            },
        ]
    },
    plugins: [
        new CopyWebpackPlugin({
                patterns: [
                    {
                        from: path.resolve(__dirname, "lib"),
                        to: path.resolve(__dirname, '.webpack/renderer/lib')
                    },
                    {
                        from: path.resolve(__dirname, "static"),
                        to: path.resolve(__dirname, '.webpack/static')
                    },
                ]
            })
    ]
}