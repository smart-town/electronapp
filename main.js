const {app, BrowserWindow, Menu, ipcMain} = require('electron')
const {createTray} = require("./main/tray")
const global = require("./main/global")
const path = require('path')
const { initialIPC } = require('./main/ipcDeal')
const { initialUpdater } = require('./main/update')

const appRoot = app.getAppPath()
const mainEntry = MAIN_WINDOW_WEBPACK_ENTRY
const {icon} = global
function genEntry() {
    const config = global.config
    return `${config.app === 'app' ? 'index' : config.app}.html`
}

function createWindow() {
    const win = new BrowserWindow({
        width: 1000,
        height: 600,
        webPreferences: {
            preload: path.join(appRoot, 'main', 'preload.js')
        }
    })
    // win.loadFile(mainEntry)
    win.loadURL(mainEntry)
    console.log(">>>load:", mainEntry)
    // win.loadFile(genEntry())
    win.setIcon(icon)
    global.config.mainWindow = win
}


app.whenReady().then(() => {
    createWindow()
    createTray()
    initialIPC()
    // initialUpdater()
    app.on('activate', () => {
        console.log("=====activate triggered=====")
        if (BrowserWindow.getAllWindows().length === 0) {
            createWindow()
            initialIPC()
            // initialUpdater()
        }
    })
})

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

const menuMap = {
    'app': 'rapp',
    'rapp': 'vapp',
    'vapp': 'app',
}

const menu = Menu.buildFromTemplate([
    {
        click: (menuItem, browserWindow, event) => {
            const config = global.config
            config.app = menuMap[config.app ? config.app : 'vapp']
            browserWindow.loadFile(genEntry(config.app))
        },
        label: '🎈',
        toolTip: '切换应用',
    },
    {
        role: 'toggleDevTools',
        label: '🐛',
    },
    {
        role: 'reload',
        label: '↺',
    },
    {
        label: '📬',
        click: () => {
            initialUpdater()
        }
    }
])

const globalData = {
    menu: null
}

Menu.setApplicationMenu(menu)
globalData.menu = Menu.getApplicationMenu()
