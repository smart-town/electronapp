import { Page1 } from "./Page1"
import React from 'react'
import ReactDOM from 'react-dom'
function App() {
    return (<div className="d-flex justify-content-center">
        {Page1()}
    </div>)
}
ReactDOM.render(<App/>, document.getElementById("root"))