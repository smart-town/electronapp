import { useEffect, useState } from "react"

const hello = [
    'welcome!',
    'I AM hhg',
    'another!',
    'ok!',
    'begin',
]
let count = 0
export function Page1() {
    const [data, setData] = useState('hello')
    const [imgSrc, setImgSrc] = useState('https://www.dmoe.cc/random.php')
    useEffect(() => {
    }, [])
    count+=1
    return (<div className="p-2 d-flex justify-content-between w-100 w-sm-50">
        <div className="card w-100">
            <img src={imgSrc} className="card-img-top"></img>
            <div className="card-body">
                <div className="card-text">{data}</div>
                <div className="d-flex justify-content-end">
                <a href="#" onClick={() => setData(hello[count % hello.length])}className="btn btn-primary btn-sm">
                    welcome
                </a>
                </div>
            </div>
        </div>
    </div>)
}